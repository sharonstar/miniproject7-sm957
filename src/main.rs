use qdrant_client::prelude::*;
use qdrant_client::qdrant::{CreateCollection, VectorParams};
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::VectorsConfig;
use qdrant_client::prelude::Distance;
use qdrant_client::client::Payload as QdrantPayload;
use rand::Rng;
use anyhow::Result;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct Payload {
    average: f32,
}

#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    let collection_name = "week7";

    client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 10,    // set vector size
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
            ..Default::default()
        }),
        ..Default::default()
    }).await?;

    // Ingest data into Vector database
    for i in 0..10 {
        let vector = generate_vector(10);
        let average: f32 = vector.iter().sum::<f32>() / vector.len() as f32;

        let mut payload: QdrantPayload = QdrantPayload::new();
        payload.insert("average".to_string(), serde_json::json!(average));

        let points = vec![PointStruct::new(i, vector, payload)];
        client.upsert_points(collection_name, None, points, None).await?;
    }

    // Perform queries and aggregations
    let query_vector: Vec<f32> = vec![0.5; 10];
    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: query_vector,
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;

    // Visualize output
    println!("Search Results:");
    for (index, point) in search_result.result.iter().enumerate() {
        // Adjusted formatting here
        println!("Point {}: ID = {:?}, Payload = {:?}", index + 1, point.id, point.payload);
    }
    Ok(())
}

fn generate_vector(size: usize) -> Vec<f32> {
    let mut rng = rand::thread_rng();
    (0..size).map(|_| rng.gen()).collect()
}

