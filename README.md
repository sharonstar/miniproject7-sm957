# miniProject7-sm957

### Requirements

Data Processing with Vector Database

- Ingest data into Vector database
- Perform queries and aggregations
- Visualize output

### Steps

1. Create a New Rust Project

    `cargo new name`
2. Add all dependencies to Cargo.toml

3. Write mian.rs. Add functions to connect with Vector database, ingest data into Vector database, and send a query to the database

```
    // Ingest data into Vector database
    for i in 0..10 {
        let vector = generate_vector(10);
        let average: f32 = vector.iter().sum::<f32>() / vector.len() as f32;

        let mut payload: QdrantPayload = QdrantPayload::new();
        payload.insert("average".to_string(), serde_json::json!(average));

        let points = vec![PointStruct::new(i, vector, payload)];
        client.upsert_points(collection_name, None, points, None).await?;
    }
```
```
    // Perform queries and aggregations
    let query_vector: Vec<f32> = vec![0.5; 10];
    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: query_vector,
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;
```

4. Launch qdrant database using Docker image

    `docker run -p 6334:6334 qdrant/qdrant`

### Screenshots
- Launch qdrant database using Docker image

![](pic/pic1.png)

- Visiual Outputs

![](pic/pic2.png)
